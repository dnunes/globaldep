'use strict';

let _instances = {};

let globalDep = function (name) {
  if (!name) { throw new Error('You must specify the dependency you want to load!'); }
  let dep = _instances[name];
  if (!dep) { throw new Error('Dependency "'+ name +'" not registered!'); }
  return dep;
};
globalDep.get = globalDep;
globalDep.register = function (name, object) {
  return (_instances[name] = object); //save reference and return ok.
};
globalDep.registerMultiple = function (objects) {
  return objects.forEach((cur) => this.register(...cur));
};

module.exports = globalDep;
