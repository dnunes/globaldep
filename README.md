globaldep
=========
### Global Dependency for DI and easy access to singletons or configured libraries across your project.

## <a id="installation">Installation</a>
The simplest way to install this package is using [npm](http://www.npmjs.com/):
```bash
$ npm i ??? -S
```

## <a id="quickstart">Quick Start Guide</a>

There is just X steps needed to start using this package:

1. X
2. X
3. PROFIT!


## <a id="methods">Methods</a>

???


## <a id="advancedusage">Advanced Usage</a>

???


## <a id="credits">Credits</a>

Created and maintained (with much ♡) by [diego nunes](http://dnunes.com)

Donations with Bitcoin to _1PQyeHqusUj3SuTmw6DPqWSHptVHkYZ33R_:

![1PQyeHqusUj3SuTmw6DPqWSHptVHkYZ33R](http://chart.apis.google.com/chart?cht=qr&chs=200x200&chl=bitcoin:1PQyeHqusUj3SuTmw6DPqWSHptVHkYZ33R)
